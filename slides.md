---
title: Rust for JS Developers
separator: <!--s-->
verticalSeparator: <!--v-->
theme: css/theme/blood.css
---

<style>
.reveal h2 {
  text-shadow: 2px 2px 10px #222 !important;
}
.reveal h1 {
  font-size: 3.1em !important;
}
.reveal ul {
  background-color: hsla(0, 10%, 12%, 0.7);
  border-radius: 0.5em;
  padding: 0.5em 1em 0.5em 1.5em;
}

.backdrop {
  display: inline-block;
  background-color: hsla(0, 10%, 12%, 0.6);
  border-radius: 0.25em;
  padding: 0.25em 0.5em !important;
  text-align: left;
}
.backdrop--red {
  background-color: hsla(0, 50%, 15%, 0.7);
}

.small {
  font-size: 0.75em !important;
}

.horizontal {
  display: flex;
  justify-content: space-between;
}

.bird {
  transform: translateY(-1em);
  position: fixed;
  top: 0;
  left: 0;
  font-size: 0.6em !important;
  display: inline-block;
  background-color: hsla(0, 10%, 12%, 0.6);
  border-radius: 0.25em;
  padding: 0.25em 0.5em !important;
  text-align: left;
}

.bird::after {
  padding-left: 0.5em;
  content: "🐦";
}
</style>

<!-- .slide: data-background="/images/photo-1467319252055-2841f61f99a2.jpg" -->

# Rust for JavaScript Developers
 
Getting a bit rusty on systems programming?

<!--v-->

# Danny Fritz

* 🏢 Lead Instructor at Galvanize
* 💻 Co-Organizer of the Denver Rust Meetup
* 🎉 Fan of Emoji
* <div class="fragment highlight-red">🐦 Appreciator of Birds</div>

<!--s-->

<!-- .slide: data-background="/images/photo-1427985841921-acba1301576e.jpg" -->

# Why Learn Rust?

<img style="height:20vh;" data-src="images/Rust_programming_language_black_logo.svg.png">

<span class="backdrop">We have reasons other than it is awesome.</span>
<span class="backdrop">Mozilla is currently rewriting the rendering engine
of Firefox in Rust. They are calling it Servo.</span>

<!--v-->

<!-- .slide: data-background="/images/photo-1428448816382-91cedd04e38a.jpg" -->

<div class="bird">Anna's Hummingbird</div>

## Rust is low-level

  * Outputs assembly, LLVM, etc.
  * Requires no runtime
  * You have control over memory

<span class="fragment fade-in">Only requires a CPU</span>
<span class="fragment fade-in">, and memory</span>
<span class="fragment fade-in">, and a bus</span>
<span class="fragment fade-in">. Pretty much a Von Neumann machine</span>
<span class="fragment fade-in">. A computer is all I'm trying to say. Leave me alone.</span>

<!--v-->

<!-- .slide: data-background="/images/photo-1429152113244-c5e6e2ed6ac9.jpg" -->

<div class="bird">Turkey Vulture</div>

## Rust is lightweight

  * No garbage collector
  * Compiler guarantees memory safety
  * Executables are smaller
  * Opportunity to run faster

<!--v-->

<!-- .slide: data-background="/images/u3x7cekkS16ajjtJcb5L_DSC_5869.jpg" -->

<div class="bird">Black Tailed Gull</div>

## Rust is not C/C++

  * C and C++ have a lot of baggage over the decades
  * It is really hard to write memory unsafe code in Rust
  * C and C++ do not care about memory unsafe code
  * Rust directly targets the C and C++ space

<span class="backdrop backdrop--red fragment fade-in">Do not murder me after this talk about these slides</span>

<!--s-->

<!-- .slide: data-background="/images/photo-1427835569204-a54780340cb5.jpg" -->

# Application vs Systems Programming

<div class="backdrop">We're trying to solve different things here!</div>

<!--v-->

<!-- .slide: data-background="/images/photo-1428572509712-cb9a529e81d7.jpg" -->

<div class="bird">Mallard</div>

## Application and Web Programming

  * API's you use are high level
  * Uses presentation languages such as HTML/CSS and XAML
  * Uses memory managed programming languages such as JavaScript and C#
  * Doesn't need to be fast
  * Needs to be able to handle errors gracefully

<!--v-->

<!-- .slide: data-background="/images/photo-1447015993193-3f72d500c3fb.jpg" -->

<div class="bird">Pied Kingfisher</div>

## Systems Programming

  * API's you use are low level
  * Uses limited resources
  * Should not have bugs
  * Needs to be fast

<span class="backdrop">Imagine a heart monitor, a satellite, or an OS. Probably don't want JavaScript running on those.</span>

<!--v-->

## Rust sits on the systems side

---

## JavaScript sits on the applications side 

<!--s-->

## Let us proceed by comparing Rust and JavaScript

<iframe src="//giphy.com/embed/ngpnmLw9cIjgk?html5=true&hideSocial=true" width="480" height="276" frameborder="0" class="giphy-embed" allowfullscreen=""></iframe>

<!--s-->

<!-- .slide: data-background="/images/photo-1474511269983-e7d42de0f2f5.jpg" -->

<div class="bird">Burrowing Owl</div>

## Installation

### Rust

```sh
curl -sSf https://static.rust-lang.org/rustup.sh | sh
```

### Node

<span class="backdrop small">Follow installer directions at https://nodejs.org/en/download/</span>

---

<span class="backdrop small">You can also use a package manager such as brew or choco</span>

<!--s-->

<!-- .slide: data-background="/images/photo-1472345113808-03a78a564cc5.jpg" -->

<div class="bird">Ash-Throated Flycatcher</div>

## Hello World 

### Rust

```rust
// hello.rs
fn main () {
  println!("chirp chirp!");
}
```

### Node

```js
// hello.js
console.log("chirp chirp!")
```

<!--s-->

<!-- .slide: data-background="/images/photo-1444465146604-4fe67bfac6e8.jpg" -->

<div class="bird">Barn Swallow</div>

## Running Programs

### Rust

```sh
rustc hello.rs && ./hello
```

<div class="backdrop">Rust `hello world!` is about 0.5Mb.</div>

### Node

```sh
node hello.js
```

<div class="backdrop">JavaScript `hello world!` is about 22b + 20Mb Node exe.</div>

<!--s-->

## Package Management

### Rust

Use cargo and a `Cargo.toml` file.

### Node

Use npm and a `package.json` file.

<!--v-->

## npm (npmjs.org)

* Scaffold project: `npm init`
* Install dependency: `npm install -S bread-crumbs`
* Fetch dependencies: `npm install`

### `package.json`

```json
{
  "name": "pigeon",
  "version": "10.3.1",
  "description": "An often disliked, but lovable bird.",
  "dependencies": {
    "bread-crumbs": "4.2.x",
  }
}
```

<!--v-->

## Cargo (crates.io)

* Scaffold project: `cargo new`
* Install dependency: Modify `Cargo.toml` manually
* Fetch dependencies and build: `cargo build`

### `Cargo.toml`

```toml
[package]
name = "seagull"
version = "0.1.0"

[dependencies]
beach = "0.1.12"
```


<!--s-->

<!-- .slide: data-background="/images/photo-1472787563973-2f710070695d.jpg" -->

<div class="bird">Cape White-Eye</div>

## Class Syntax

<!--v-->

#### JavaScript has objects with values attached:

```js
// Data and methods live here
class Bird {
  constructor (name) {
    this.name = name
  }
  chirp () {
    return this.name + ": chirp chirp chirp"
  }
}

let robin = new Bird("Robin")
console.log(robin.chirp()) // Robin: chirp chirp chirp
```

<!--v-->

#### Rust has structs with associated methods:

```rust
// Data lives here
struct Bird {
    name: String
}

// Methods live here
impl Bird {
    fn chirp(&self) -> String {
        let mut chirp = self.name.to_owned();
        chirp.push_str(": chirp chirp chirp. ");
        return chirp;
    }
}

fn main () {
    let crow = Bird{ name: "Crow".to_owned() };
    println!("{}", crow.chirp()); // Crow: chirp chirp chirp
}
```

---

<span class="backdrop small">Even in this simple example you can see
Rust is much more concerned with handling memory.</span>

<!--s-->

<!-- .slide: data-background="/images/photo-1444465693019-aa0b6392460d.jpg" -->

<div class="bird">Finch LBJ</div>

## Memory Model

<!--v-->

#### JavaScript garbage collects and cares very little about references

```js
var magpie = { name: "magpie" }
var stellarJay = magpie
stellarJay.name = "Stellar Jay"
console.log(magpie.name) // => "Stellar Jay"
stellarJay = undefined
console.log(stellarJay.name)
// Uncaught TypeError: Cannot read property 'name' of undefined
```

<div class="backdrop fragment fade-in">Pretty much do whatever the heck you feel like.
Zero shits are given by JavaScript in regard to how you manage memory.
However, if you suck at it, JavaScript will error at runtime. Fun.</div>

<span class="fragment fade-in backdrop backdrop--red">Once again, please do not kill me after this talk for these slides</span>

<!--v-->

#### Rust does not garbage collect and cares very deeply about references

```rust
struct Bird {
    name: String
}
fn main () {
    let warbler = Bird{ name: "Warbler".to_owned() };
    let yellowWarbler = warbler;
    println!("{}", warbler.name);
}
```


<img data-src="images/atom_2016-11-15_22-00-44.png">
  
<div class="backdrop fragment fade-in">Rust gets all up in your
shit during compilation to make sure you are following best memory
practices.</div>

<!--s-->

<!-- .slide: data-background="/images/photo-1468493424329-9c14ef65590c.jpg" -->

<div class="bird">Eastern Yellow Robin</div>

## Modules

<!--v-->

#### Node uses CommonJS to handle modules

```js
// Bird.js
module.exports = class Bird {}
```

```js
// index.js
const Bird = require("./Bird.js")
const towhee = new Bird()
```

<!--v-->

#### Rust uses `mod` to handle modules

```rust
// bird/mod.rs
pub struct bird {}

impl bird {}

pub fn new () -> bird {
    bird{}
}
```

```rust
// main.rs
mod bird; // knows to look in bird/mod.rs
fn main () {
    let sparrow = bird::new();
}
```

<div class="backdrop fragment fade-in">Rust's `mod` keyword does go much deeper than this.</dvi>

<!--s-->

## More feature of Rust

  * Enums
  * Match
  * Guard
  * Generics
  * Ownership / Borrowing / Lifetimes
  * Traits
  * Visibility
  * Threads / Channels
  * FFI
  * Closures
  * etc... So many...
  

<!--v-->

## Some more ideas

* [Neon](https://github.com/rustbridge/neon): Run Rust programs inside your Node applications
* [emscripten](http://kripken.github.io/emscripten-site/) and [binaryen](https://github.com/WebAssembly/binaryen): Compile rust to WebAssembly and run Rust in the browser
* [Piston](http://www.piston.rs/) and [Amethyst](https://github.com/amethyst/amethyst): Make a video game
* [HTTP](http://www.arewewebyet.org/): Make a web server
* [Machine Learning](http://www.arewelearningyet.com/): Destroy the world with AI

<!--s-->

<!-- .slide: data-background="/images/photo-1436731837106-3b37ef132fc2.jpg" -->

<div class="bird">American Robin Eggs</div>

## Moral: Don't Forget About Systems

<div class="backdrop">Don't put all your eggs in one basket. Learn Rust!</div>

<span class="fragment fade-in backdrop backdrop--red">Bad jokes are also not a good reason to hurt me.</span>

<!--s-->

<!-- .slide: data-background="/images/photo-1433888376991-1297486ba3f5.jpg" -->

<div class="bird">Raven</div>

# Thank You!

<div class="backdrop">dannyfritz@gmail.com</div>
<div class="backdrop">dannyfritz.com</div>
